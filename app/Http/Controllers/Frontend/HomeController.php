<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use DB;
use Carbon\Carbon;

use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
  // Page - Home
  public function index()
  {
    return view('frontend.pages.home');
  }

  // Page - Contato
  public function contact()
  {
    return view('frontend.contact');
  }
  // Page - Contato
  public function contactSendMail(Request $request)
  {
    $rules = [
      'name'     => "required",
      'email' => 'required|email',
      'subject' => 'required',
      'message' => 'required'
    ];

    $messages = [
      'name.required' => 'nome é obrigatório',
      'email.required' => 'email é obrigatório',
      'subject.required' => 'assunto é obrigatório',
      'message.required' => 'mensagem é obrigatório',
    ];
    
    $validator = Validator::make($request->all(), $rules, $messages);
    
    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }
    
    try {
      Mail::send(new SendMail($request));
    } catch (\Exception $e) {
      Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('E-mail encaminhado com sucesso', 200);
  }
}
