<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});

Route::get('/quem-somos', function () {
    return view('frontend.about');
});

Route::get('/nossos-servicos', function () {
    return view('frontend.services');
});

Route::get('/portfolio', function () {
    return view('frontend.portfolio');
});

Route::get('/contato', 'Frontend\HomeController@contact')->name('frontend.contact');
Route::post('/contato-sendmail', 'Frontend\HomeController@contactSendMail')->name('frontend.contact.sendmail');


// servicos     
Route::get('/administracao-b2wads', function () {
    return view('frontend.services.administracao-b2wads');
});
Route::get('/campanha-google-ads', function () {
    return view('frontend.services.campanha-google-ads');
});
Route::get('/desenvolvimento-sites', function () {
    return view('frontend.services.desenvolvimento-sites');
});
Route::get('/gerenciamento-erp', function () {
    return view('frontend.services.gerenciamento-erp');
});
Route::get('/marketing-conteudo', function () {
    return view('frontend.services.marketing-conteudo');
});
Route::get('/otimizacao-site-seo', function () {
    return view('frontend.services.otimizacao-site-seo');
});
Route::get('/registro-dominio', function () {
    return view('frontend.services.registro-dominio');
});
Route::get('/ecommerce-loja-virtual', function () {
    return view('frontend.services.ecommerce-loja-virtual');
});


