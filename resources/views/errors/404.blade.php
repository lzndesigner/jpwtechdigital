@extends('frontend.base')

@section('title', __('Not Found'))
@section('content')
<div id="page-error" class="page-section padding-100-0">

    <div class="title-page divcenter center bottommargin font-body" style="max-width: 850px;">
        <img src="{{ asset('/galerias/paginas/errors/404.png?1') }}" alt="Página não encontrada">

        <div class="divider divider-center"><i class="icon-cloud"></i></div>
        <h2 class="nobottommargin t600 ls1">Página não Encontrada</h2>
    </div>


</div>
@endsection