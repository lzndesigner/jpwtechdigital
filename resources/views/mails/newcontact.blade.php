Nome: {{$email->name}} <br>
E-mail: {{$email->email}} <br>
@if(isset($email->phone))
Telefone: {{$email->phone}} <br>
@endif
@if(isset($email->service))
Serviço: {{$email->service}} <br>
@endif
@if(isset($email->reference))
Referencia: {{$email->reference}} <br>
@endif
@if(isset($email->subject))
Assunto: {{$email->subject}} <br>
@endif
Mensagem: {{$email->message}} <br> <br>
Data: {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}