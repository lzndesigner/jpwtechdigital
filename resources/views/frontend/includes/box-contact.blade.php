<a href="/contato" class="button button-full button-dark center text-right">
    <div class="container clearfix">
        Fale com nossos especialistas, tire suas dúvidas, <br> estamos ansiosos em atender-lo. <strong>Entre em Contato</strong>
        <i class="icon-caret-right" style="top:4px;"></i>
    </div>
</a>