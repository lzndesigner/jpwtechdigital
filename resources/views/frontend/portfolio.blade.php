@extends('frontend.base')
@section('title', 'Portfólio')

@section('content')
    <div id="section-works" class="page-section">

        <div class="title-page divcenter center bottommargin font-body" style="max-width: 850px;">
            <h2 class="nobottommargin t600 ls1">Casos de Clientes</h2>
            <div class="divider divider-center"><i class="icon-cloud"></i></div>
        </div>

        <div class="container">

            <!-- Portfolio Filter ============================================= -->
            <ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">

                <li class="activeFilter"><a href="#" data-filter="*">Todos</a></li>
                <li><a href="#" data-filter=".pf-sites">Sites</a></li>
                <li><a href="#" data-filter=".pf-lojavirtual">Lojas Virtuais</a></li>

            </ul><!-- #portfolio-filter end -->

            <div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
                <i class="icon-random"></i>
            </div>

            <div class="clear"></div>


            <!-- Portfolio Items ============================================= -->
            <div id="portfolio" class="portfolio grid-container portfolio-3 clearfix bottommargin">



                <article class="portfolio-item pf-media pf-sites">
                    <div class="portfolio-image">
                        <a href="https://koisas.com.br" target="_Blank">
                            <img src="{{ asset('/galerias/portfolio/koisas.png') }}" alt="Loja - Koisas">
                        </a>
                        <div class="portfolio-overlay">
                            <a href="{{ asset('/galerias/portfolio/koisas.png') }}" class="left-icon"
                                data-lightbox="image" title="Zoom na Imagem" data-toggle="tooltip"><i
                                    class="fa fa-search"></i></a>
                            <a href="https://koisas.com.br" target="_Blank" class="right-icon" title="Abrir página"
                                data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a href="https://koisas.com.br" target="_Blank">Loja - Koisas</a></h3>
                        <span><a>E-commerce</a></span>
                    </div>
                </article>
                <article class="portfolio-item pf-media pf-sites">
                    <div class="portfolio-image">
                        <a href="https://lavanderiastylo.com.br" target="_Blank">
                            <img src="{{ asset('/galerias/portfolio/lavanderiastylo.png') }}"
                                alt="Site - Lavanderia Stylo">
                        </a>
                        <div class="portfolio-overlay">
                            <a href="{{ asset('/galerias/portfolio/lavanderiastylo.png') }}" class="left-icon"
                                data-lightbox="image" title="Zoom na Imagem" data-toggle="tooltip"><i
                                    class="fa fa-search"></i></a>
                            <a href="https://lavanderiastylo.com.br" target="_Blank" class="right-icon" title="Abrir página"
                                data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a href="https://lavanderiastylo.com.br" target="_Blank">Site - Lavanderia Stylo</a></h3>
                        <span><a>Site Institucional</a></span>
                    </div>
                </article>

                <article class="portfolio-item pf-media pf-lojavirtual">
                    <div class="portfolio-image">
                        <a href="https://moveiscasarustica.com.br" target="_Blank">
                            <img src="{{ asset('/galerias/portfolio/moveiscasarustica.png') }}"
                                alt="Loja - Móveis Casa Rústica">
                        </a>
                        <div class="portfolio-overlay">
                            <a href="{{ asset('/galerias/portfolio/moveiscasarustica.png') }}" class="left-icon"
                                data-lightbox="image" title="Zoom na Imagem" data-toggle="tooltip"><i
                                    class="fa fa-search"></i></a>
                            <a href="https://moveiscasarustica.com.br" target="_Blank" class="right-icon"
                                title="Abrir página" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a href="https://moveiscasarustica.com.br" target="_Blank">Loja - Móveis Casa Rústica</a></h3>
                        <span><a>E-commerce</a></span>
                    </div>
                </article>

                <article class="portfolio-item pf-media pf-sites">
                    <div class="portfolio-image">
                        <a href="/portfolios/lavanderia-alves">
                            <img src="{{ asset('/galerias/portfolio/lavanderiaalves.png') }}"
                                alt="Monitor - Lavanderia Alves">
                        </a>
                        <div class="portfolio-overlay">
                            <a href="{{ asset('/galerias/portfolio/lavanderiaalves.png') }}" class="left-icon"
                                data-lightbox="image" title="Zoom na Imagem" data-toggle="tooltip"><i
                                    class="fa fa-search"></i></a>
                            <a href="/portfolios/lavanderia-alves" class="right-icon" title="Abrir página"
                                data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a href="/portfolios/lavanderia-alves">Monitor - Lavanderia Alves</a></h3>
                        <span><a>Sistema Personalizado</a></span>
                    </div>
                </article>

            </div><!-- #portfolio end -->

        </div><!-- container -->

        @include('frontend.includes.box-contact')

    </div><!-- page-section -->
@endsection
