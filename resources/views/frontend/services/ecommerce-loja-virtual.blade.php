@extends('frontend.base')
@section('title', 'Loja Virtual - Ecommerce')

@section('content')
<div class="page-section nobottompadding">
    <div class="container">
        <div class="title-page divcenter center bottommargin font-body" style="max-width: 650px;">
            <h2 class="nobottommargin t600 ls1">Loja Virtual <small>(E-commerce)</small></h2>
            <span>Nossa proposta vai muito além de entrega uma loja online... <br> Vamos acompanhar e orientar você durante sua jornada no Mundo Digital.</span>
            <div class="divider divider-center"><i class="icon-cloud"></i></div>
        </div>
    </div>

    <div class="container bottommargin-sm">
        <div class="row align-items-center col-mb-50">
            <div class="col-md-5">
                <img src="galerias/paginas/ecommerce.png" alt="Loja Virtual E-commerce" class="img-responsive">
            </div>
            <div class="col-md-7 text-left text-md-left">
                <div class="heading-block bottommargin-sm">
                    <h4>Como funciona uma Loja Virtual?</h4>
                </div>
                <p>A venda pela internet tem aumentado nos últimos tempos, com uma Loja virtual própria, você pode vender seus produtos em um ambiente seguro, com o visual totalmente personalizado com o nome da sua Loja, cores e todas as informações necessárias para que o cliente possa identificar a loja, conhecer os produtos, finalizar pedidos diretamente pela loja e muitas outras funciolidades.</p>
                <p>O site possuí um sistema robusto com diversos recursos básicos para que você alavancar suas vendas de forma rápida e fácil, os clientes e novos visitantes podem acessar através do seu domínio (www.sualoja.com.br) utilizando computadores, celulares e outros dispositivos. Mecanismo otimizado para S.E.O com URLs amigáveis automaticas para que o Google e outros buscadores encontrem sua loja e leve mais clientes que buscam pelo seu produto.</p>

            </div>
        </div>

        <div class="line"></div>


        <div class="row">
            <div class="col-md-4 side-text-right-container side-text-plan-hosting">
                <h2 class="side-text-right-title f-size25">O que vamos entregar?</h2>
                <ul class="web-hosting-options">
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> Funcionamento 24 horas por dia</li>
                    <li><i class="fa fa-cogs" aria-hidden="true"></i> Instalação &amp; Configuração</li>
                    <li><i class="fa fa-youtube" aria-hidden="true"></i> Treinamento para iniciar</li>
                    <li><i class="fab fa-whatsapp" aria-hidden="true"></i> Suporte pelo WhatsApp</li>
                    <li><i class="fab fa-expeditedssl" aria-hidden="true"></i> Certificado de Segurança SSL</li>
                    <li><i class="fas fa-desktop" aria-hidden="true"></i> Layout exclusivo e responsivo</li>
                    <li><i class="fas fa-search" aria-hidden="true"></i> Otimização de site - S.E.O</li>
                    <li><i class="fa fa-database" aria-hidden="true"></i> Backup &amp; Manutenção</li>
                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Sem contrato de Fidelidade</li>
                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Sem taxas % de Comissão</li>
                </ul>
            </div>
            <div class="col-md-8 side-text-right-container side-text-plan-hosting">
                <h2 class="side-text-right-title f-size25">Recursos Padrões</h2>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="web-hosting-options">
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Atacado &amp; Varejo</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Simulação de Frete com os Correios</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Pagamento por Depósito, Cartões e Boleto</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Produtos com Opções de Variações</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Marcas e Comentários</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Integração com Redes Sociais</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="web-hosting-options">
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Páginas Institucionais e de Informações</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Programa de Afiliados</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Cupons de Descontos</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Informativo ao Cliente</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Relatórios de Vendas, Produtos e Clientes</li>
                                    <li><i class="fa fa-remove text-success" aria-hidden="true"></i> Imagens de produtos com Zoom</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">


        <h2 class="side-text-right-title f-size25">Nossos Clientes</h2>

        <!-- Portfolio Items ============================================= -->
        <div id="portfolio" class="portfolio grid-container portfolio-3 clearfix bottommargin">

            <article class="portfolio-item pf-media pf-lojavirtual">
                <div class="portfolio-image">
                    <a href="https://floranatus.com.br" target="_Blank">
                        <img src="{{ asset('/galerias/portfolio/floranatus.png') }}" alt="Loja - Floranatus">
                    </a>
                    <div class="portfolio-overlay">
                        <a href="{{ asset('/galerias/portfolio/floranatus.png') }}" class="left-icon" data-lightbox="image" title="Zoom na Imagem" data-toggle="tooltip"><i class="fa fa-search"></i></a>
                        <a href="https://floranatus.com.br" target="_Blank" class="right-icon" title="Abrir página" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                    </div>
                </div>
                <div class="portfolio-desc">
                    <h3><a href="https://floranatus.com.br" target="_Blank">Loja - Floranatus</a></h3>
                    <span><a>E-commerce</a></span>
                </div>
            </article>

            <article class="portfolio-item pf-media pf-lojavirtual">
                <div class="portfolio-image">
                    <a href="https://moveiscasarustica.com.br" target="_Blank">
                        <img src="{{ asset('/galerias/portfolio/moveiscasarustica.png') }}" alt="Loja - Móveis Casa Rústica">
                    </a>
                    <div class="portfolio-overlay">
                        <a href="{{ asset('/galerias/portfolio/moveiscasarustica.png') }}" class="left-icon" data-lightbox="image" title="Zoom na Imagem" data-toggle="tooltip"><i class="fa fa-search"></i></a>
                        <a href="https://moveiscasarustica.com.br" target="_Blank" class="right-icon" title="Abrir página" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                    </div>
                </div>
                <div class="portfolio-desc">
                    <h3><a href="https://moveiscasarustica.com.br" target="_Blank">Loja - Móveis Casa Rústica</a></h3>
                    <span><a>E-commerce</a></span>
                </div>
            </article>


        </div><!-- #portfolio end -->

    </div><!-- container -->

	
	@include('frontend.includes.box-contact')
    

</div><!-- section -->
@endsection