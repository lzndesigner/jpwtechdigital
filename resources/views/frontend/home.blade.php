@extends('frontend.base')
@section('title', 'Bem vindo')

@section('content')

<section id="slider" class="slider-parallax swiper_wrapper clearfix" data-autoplay="5000" data-speed="300" data-effect="fade" data-loop="true">

	<div class="swiper-container swiper-parent">
		<div class="swiper-wrapper">
			<div class="swiper-slide dark" style="background-image: url('/galerias/slider/campanha_ads.jpg?1');">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h4 data-caption-animate="fadeInUp">A principal ferramenta de anúncios através da internet</h4>
						<h2 data-caption-animate="fadeInUp">Campanha <br> Google Ads</h2>
						<p data-caption-animate="fadeInUp" data-caption-delay="200">
							Planejamento, Criação e Gerenciamento Estratégico de Campanhas Especialistas certificados pelo Google
						</p>
						<a href="{{ url('/campanha-google-ads') }}" class="btn btn-lg btn-white-border" data-caption-animate="fadeInUp" data-caption-delay="350">CONHEÇA MAIS</a>
					</div>
				</div>
			</div><!-- swiper -->
			<div class="swiper-slide dark" style="background-image: url('/galerias/slider/criacao_site.jpg?1');">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h4 data-caption-animate="fadeInUp">Sites responsivos, compatíveis com todos os dispositivos móveis</h4>
						<h2 data-caption-animate="fadeInUp">Criação de Sites <br> Profissionais</h2>
						<p data-caption-animate="fadeInUp" data-caption-delay="200">
							Desenvolvimento em linguagens de programação modernas e pré-otimizados para os mecanismos de buscas. Registro de domínios e hospedagem em servidor dedicado de alta performance e segurança.
						</p>
						<a href="{{ url('/desenvolvimento-sites') }}" class="btn btn-lg btn-white-border" data-caption-animate="fadeInUp" data-caption-delay="350">CONHEÇA MAIS</a>
					</div>
				</div>
			</div><!-- swiper -->
			<div class="swiper-slide dark" style="background-image: url('/galerias/slider/seo.jpg?1');">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h4 data-caption-animate="fadeInUp">Aumente o posicionamento nos resultados de buscas</h4>
						<h2 data-caption-animate="fadeInUp">S.E.O Otimização de Site</h2>
						<p data-caption-animate="fadeInUp" data-caption-delay="200">
							Técnicas avançadas aplicadas no código-fonte e no conteúdo contextual do site, otimizando-o para aumentar o posicionamento (ranking) nos resultados das buscas orgânicas.
						</p>
						<a href="{{ url('/otimizacao-site-seo') }}" class="btn btn-lg btn-white-border" data-caption-animate="fadeInUp" data-caption-delay="350">CONHEÇA MAIS</a>
					</div>
				</div>
			</div><!-- swiper -->
		</div>
		<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
		<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
		<div id="slide-number">
			<div id="slide-number-current"></div><span>/</span>
			<div id="slide-number-total"></div>
		</div>
	</div>

</section>

<section id="content">
	<div class="content-wrap">

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="heading-block center border-bottom-0">
						<h2>Uma empresa de tecnologia digital <br> que cria experiências impressionantes</h2>
						<span>Entregamos soluções multiplataformas ajudando sua empresa a focar em potênciais clientes com melhores resultados de busca.</span>
					</div>
				</div>
			</div>
		</div><!-- container -->

		<div class="container clearfix">

			<div class="col-xs-12 col-md-4 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/desenvolvimento-sites"><i class="icon-screen i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Desenvolvimento de Sites</h4>
						<p>Um site tem como objetivo proporcionar uma comunicação eficaz, maior visibilidade e interações dos clientes com a empresa.</p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-md-4 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/otimizacao-site-seo"><i class="fa fa-chart-line i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Otimização de Sites S.E.O</h4>
						<p>Alcance o topo e seja encontrado por mais clientes nas buscas no Google, Bing, Yahoo e outros...</p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-md-4 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/campanha-google-ads"><i class="fa fa-award i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Campanhas Google ADS</h4>
						<p>Anúncios e links patrocinados são exibidos nos resultados das busca no Google de acordo com o que está sendo procurando.</p>
					</div>
				</div>
			</div>

			<div class="clear"></div>

			<div class="col-xs-12 col-md-4 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/marketing-conteudo"><i class="icon-book i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Marketing de Conteúdo</h4>
						<p>Metodologia focada em entender a forma como o consumidor pensa, com o que ele interage até o momento ideal de compra.</p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-md-4 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/registro-dominio"><i class="icon-globe i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Registro de Domínio</h4>
						<p>É basicamente o seu nome na internet, os consumidores podem encontrar você na internet digitando seu domínio.com.br</p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-md-4 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/gerenciamento-erp"><i class="icon-cube i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Gerenciamento de ERP</h4>
						<p>ERP serve para manter em um único local todos os dados e processos da empresa, desde finanças e contabilidade, e vendas.</p>
					</div>
				</div>
			</div>

			<div class="clear"></div>

			<div class="col-xs-12 col-md-6 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/administracao-b2wads"><i class="icon-cogs i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>Administração de B2WAds</h4>
						<p>Mídia patrocinada que acompanham toda a jornada de compra do cliente, proporcionando uma exposição muito mais assertiva para os seus produtos e marca.</p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-md-6 bottommargin">
				<div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect borderbottom">
					<div class="fbox-icon">
						<a href="/ecommerce-loja-virtual"><i class="icon-shopping-cart i-alt"></i></a>
					</div>
					<div class="fbox-content">
						<h4>E-commerce Loja Virtual</h4>
						<p>Ofereça seus produtos e serviços para o mundo, através de uma loja totalmente exclusiva, feita sob medida, com segurança, suporte e de fácil administração.</p>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

			<div class="clear"></div>
			<div class="line"></div>

			<h4 class="center sub-title">Nossos Clientes</h4>

			<ul class="clients-grid grid-5 bottommargin clearfix text-center">
				<li><a href="https://koisas.com.br/" target="_Blank"><img src="galerias/clientes/koisas.png" alt="Koisas"></a></li>
				<li><a href="https://moveiscasarustica.com.br/" target="_Blank"><img src="galerias/clientes/moveiscasarustica.png" alt="Moveis Casa Rústica"></a></li>
				<li><a href="https://www.seopimenta.com.br/" target="_Blank"><img src="galerias/clientes/seopimenta.png" alt="SEO Pimenta"></a></li>
				<li><a><img src="galerias/clientes/tiautiau.png" alt="Tiau Tiau"></a></li>
				<li><a href="https://limpezadesofas.com.br/" target="_Blank"><img src="galerias/clientes/lavanderia_stylo.png" alt="Lavanderia Stylo"></a></li>
			</ul>
		</div>

		<div class="section footer-stick">

			<h4 class="center sub-title">Depoimento de <span>Clientes</span></h4>

			<div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
				<div class="flexslider">
					<div class="slider-wrap">
						<div class="slide">
							<div class="testi-image">
								<a href="https://limpezadesofas.com.br/" target="_Blank"><img src="galerias/clientes/lavanderia_stylo.png" alt="Lavanderia Stylo"></a>
							</div>
							<div class="testi-content">
								<p>Estamos junto com a JPW, no agenciamento de nossas campanhas de marketing digital. Confiamos no trabalho deles e estamos colhendo varios frutos dessa parceria.</p>
								<div class="testi-meta">
									João Paulo
									<span>Lavanderia Stylo</span>
								</div>
							</div>
						</div>
						<div class="slide">
							<div class="testi-image">
								<a href="http://moveiscasarustica.com.br/" target="_Blank"><img src="galerias/clientes/moveiscasarustica.png" alt="Moveis Casa Rústica"></a>
							</div>
							<div class="testi-content">
								<p>Temos uma grande satisfação de sermos parceiros da JPW, que alem de nos alancar no segmento de e-commerce, tambem nos direciona a todo momento no aumento de nossas vendas com o agenciamento de nossas campanhas google ads.</p>
								<div class="testi-meta">
									Paulo e Deco
									<span>Moveis Casa Rustica</span>
								</div>
							</div>
						</div>
						<div class="slide">
							<div class="testi-image">
								<a href="http://seopimenta.com.br/" target="_Blank"><img src="galerias/clientes/seopimenta.png" alt="SEO Pimenta"></a>
							</div>
							<div class="testi-content">
								<p>Temos a felicidade e satisfação de sermos parceiros da JPW atualmente no segmento de e-commerce, onde tivemos grandes resultados em vendas e exposiçao de marca.</p>
								<div class="testi-meta">
									Felipe Baldin
									<span>Seo Pimenta</span>
								</div>
							</div>
						</div>
						<div class="slide">
							<div class="testi-image">
								<a><img src="galerias/clientes/tiautiau.png" alt="Tiau Tiau"></a>
							</div>
							<div class="testi-content">
								<p>Sem duvida a JPW foi um grande drive para alanvancarmos nossa visibilidade e o Lead de novos clientes. Hoje alcançamos os mercados desejados com facilidade devido a excelente gestão de nossas campanhas.</p>
								<div class="testi-meta">
									William Rodrigues
									<span>Tiau Tiau</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
	<div class="promo promo-border promo-full">
		<div class="container clearfix">
			<h3>Estamos ansiosos em atender-lo por Telefone <span>(11) 9.5284-0967</span> <br> ou nos envie um e-mail <span><a href="mailto:contato@jpwtechdigital.com.br">contato@jpwtechdigital.com.br</a></span></h3>
			<a href="{{ url('contato') }}" class="button button-xlarge button-rounded">Entrar em Contato</a>
		</div>
	</div>

	@include('frontend.includes.box-contact')
    

</section><!-- #content end -->
@endsection