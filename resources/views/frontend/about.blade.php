@extends('frontend.base')
@section('title', 'Quem Somos')

@section('content')
<div id="section-works" class="page-section">

    <div class="title-page divcenter center bottommargin font-body" style="max-width: 850px;">
        <h2 class="nobottommargin t600 ls1">Quem Somos</h2>
        <div class="divider divider-center"><i class="icon-cloud"></i></div>
    </div>

    <div class="container">

        <h2 class="divcenter bottommargin font-body text-center" style="max-width: 900px; font-size: 36px;">
            A JPW Techdigital é uma empresa especializada em soluções digitais para diversas áreas e segmentos.
        </h2>

        <p class="lead divcenter bottommargin t500 text-center" style="max-width: 700px;">
            Entregamos soluções multiplataformas ajudando sua empresa a focar em potênciais clientes com melhores resultados de buscas.
        </p>

    </div>


    <div class="container topmargin clearfix">
        <div class="divcenter text-center" style="max-width: 750px;">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4>O que nós <span>Fazemos</span>.</h4>
            </div>

            <p>Aumentamos a performance de sua empresa por meio de inovação em Marketing de Conteúdo com serviços criativos, otimização de Sites S.E.O, gerenciamento de ERP, administração de B2WAds, Registro de Domínios e amplo expertise para seu E-commerce ou Loja Virtual.</p>

        </div>
    </div>

    <div class="container topmargin clearfix">

        <div class="row">

            <div class="col-12 col-sm-6 col-md-5">

                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4>Porque escolher <span>nós</span>?</h4>
                </div>

                <p>Nós temos as melhores experiências e soluções digitais multiplataformas com o foco na máxima satisfação de nossos clientes, colaboradores e stakeholders.</p>
                <p>Para isso, criamos soluções que tornam o complexo em simples, transformam dados em inteligência e estimulam a colaboração de todos envolvidos em nossos projetos.</p>

                <div class="clearfix"></div>

                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4>Nosso <span>Propósito</span>.</h4>
                </div>

                <p>Trabalhamos com muito ENTUSIASMO acreditando que o futuro está cheio de possibilidades e oportunidades. JUNTOS assessoramos o seu negócio a PROSPERAR no mundo digital, construindo IDÉIAS e escalando RESULTADOS sustentáveis.</p>

            </div>

            <div class="col-12 col-sm-6 col-md-7 col_last">
                <div class="heading-block fancy-title nobottomborder title-bottom-border">
                    <h4>Nossos <span>Valores</span>.</h4>
                </div>

                <p>Acreditamos que é possível conciliar elevado profissionalismo com descontração, sem perder o prazer de trabalhar e servir a outros.</p>

                <ul class="list-values">
                    <li><b>Responsabilidade</b>: Nós assumimos e honramos os compromissos e suas consequências.</li>

                    <li><b>Integridade</b>: Nós agimos com transparência, ética e honestidade.</li>

                    <li><b>Respeito</b>: Nós cultivamos a consideração pelo outro independente do julgamento individual</li>

                    <li><b>Inovação</b>: Nós buscamos soluções criativas para a evolução do convencional.</li>

                    <li><b>União</b>: Nós criamos um ambiente em que o coletivo prevaleça sobre o individual.</li>

                    <li><b>Coragem</b>: Nós temos força e confiança para superar os limites.</li>

                </ul>

            </div>

        </div>

    </div>

    @include('frontend.includes.box-contact')
    
    
</div>
@endsection