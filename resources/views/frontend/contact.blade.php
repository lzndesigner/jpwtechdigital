@extends('frontend.base')
@section('title', 'Entre em Contato')

@section('content')
<div id="section-contact" class="page-section">

<div class="container clearfix">
    <div class="title-page divcenter center mb-5" style="max-width: 900px;">
        <h2 class="nobottommargin t600 ls1">Faça seu Orçamento</h2>
        <div class="divider divider-center"><i class="icon-cloud"></i></div>
    </div>
        
    <div class="divcenter topmargin" style="max-width: 850px;">							

        <div class="contact-widget">

            <div class="contact-form-result"></div>

            <form class="nobottommargin" id="form-request" method="post" action="{{ route('frontend.contact.sendmail') }}">
                @csrf
                <div id="form-messages"></div>

                <div class="col_half">
                    <input type="text" id="name" name="name" value="" class="sm-form-control border-form-control required" placeholder="Nome Completo" />
                </div>
                <div class="col_half col_last">
                    <input type="email" id="email" name="email" value="" class="required email sm-form-control border-form-control" placeholder="E-mail de Contato" />
                </div>

                <div class="clear"></div>

                <div class="col_one_third">
                    <input type="text" id="phone" name="phone" value="" class="sm-form-control border-form-control" placeholder="Telefone/Celular" />
                </div>

                <div class="col_one_third">
                    <select name="service" id="service" class="sm-form-control border-form-control">
                        <option disabled selected>Selecione um serviço</option>
                        <option value="desenvolvimento-sites">Desenvolvimento de Sites</option>
                        <option value="otimizacao-sites">Otimização de Sites S.E.O</option>
                        <option value="campanha-google">Campanha Google Ads</option>
                        <option value="marketing-conteudo">Marketing de Conteúdo</option>
                        <option value="gerenciamento-erp">Gerenciamento ERP</option>
                        <option value="registro-dominio">Registro de Domínios</option>
                        <option value="administracao-b2wads">Administração B2WADs</option>
                        <option value="loja-virtual">Loja Virtual E-commerce</option>
                        <option value="outros">Outros</option>
                    </select>
                </div>

                <div class="col_one_third col_last">
                    <select name="reference" id="reference" class="sm-form-control border-form-control">
                        <option disabled selected>Como nos conheceu?</option>
                        <option value="google">Google</option>
                        <option value="indicacao">Indicação</option>
                        <option value="outros-sites">Outros sites</option>
                        <option value="outros-meios">Outros meios</option>
                    </select>
                </div>

                <div class="clear"></div>

                <div class="col_full">
                    <input type="text" id="subject" name="subject" value="" class="required sm-form-control border-form-control" placeholder="Assunto" />
                </div>

                <div class="clear"></div>

                <div class="col_full">
                    <textarea class="required sm-form-control border-form-control" id="message" name="message" rows="5" cols="30" placeholder="Digite sua mensagem"></textarea>
                </div>

                <div class="col_full center">
                    <button  type="submit" id="btn-sendmail" class="button button-border button-circle t500 noleftmargin topmargin-sm" value="submit">Enviar Mensagem</button>
                    <br>
                    <small style="display: block; font-size: 13px; margin-top: 15px;">Faremos o possível para retornar a você dentro de 6 a 8 horas úteis.</small>
                </div>

            </form>

        </div>

    </div>

</div>

@include('frontend.includes.box-contact')

</div>
@endsection


@section('cssPage')
<link rel="stylesheet" href="{{ asset('/frontend/js/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('jsPage')
<script src="{{ asset('/frontend/js/sweetalert/sweetalert2.min.js') }}"></script>
<script>
    $(document).on('click', '#btn-sendmail', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });

        $(this).attr('disabled', true);

        var url = "{{ route('frontend.contact.sendmail') }}";
        var method = 'POST';

        var data = $('#form-request').serialize();
        $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
                Swal.fire({
                    text: data,
                    icon: 'success',
                    showClass: {
                        popup: 'animate_animated animate_backInUp'
                    },
                    onClose: () => {
                        // Loading page listagem
                        $('#form-request')[0].reset();
                        $('#btn-sendmail').attr('disabled', false);
                    }
                });
            },
            error: function(xhr) {
                if (xhr.status === 422) {
                    Swal.fire({
                        text: 'Validação: ' + xhr.responseJSON,
                        icon: 'warning',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                } else {
                    Swal.fire({
                        text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }               

                $('#btn-sendmail').attr('disabled', false);
            }
        });
    });
</script>
@endsection