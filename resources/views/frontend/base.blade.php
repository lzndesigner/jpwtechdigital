<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">

<head>

    <!-- Document Title
	============================================= -->
    <title>@yield('title') - JPW Tech Digital - Sua solução para evolução digital</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="author" content="JPW Tech Digital" />
    <meta name="description" content="Somos uma empresa de tecnologia digital e inovação, comprometida em entregar os melhores resultados aos nossos clientes. Focados em entregar soluções multiplataformas e ajudar sua empresa a se focar em seus próprios clientes em busca de melhores resultados. Desenvolvemos sua loja virtual, e-commerce, gerenciamos sua campanha google ads, aumentaremos suas vendas, gerenciamos toda sua campanha de marketing inbound, criação seu logo, seu cartão de visita, impulsionamos suas redes sociais">
    <meta name="keywords" content="jpw tech, jpw, tech, solucao, evolucao, digital, google ads, adsword, desenvolvimento de sites, SEO, marketing de conteudo, sites empresariais, venda mais, loja virutal, e-commerce" />
    <meta name="robots" content="index, follow" />
    <link href="{{ asset('/galerias/favicon.ico') }}" rel="icon">

    <!-- Stylesheets
	============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/style.css?2') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/swiper.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/dark.css?2') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/magnific-popup.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/custom.css?'.rand()) }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/custom.css?'.rand()) }}" type="text/css" />
    @yield('cssPage')
    <link rel="stylesheet" href="{{ asset('/fontawesome/css/all.min.css') }}" type="text/css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-UA-169354743-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-169354743-1');
    </script>

</head>

<body class="stretched {{ (collect(request()->segments())->last() == '') ? 'home' : collect(request()->segments())->last() }}">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
		============================================= -->
        <header id="header" class="sticky-style-2">

            <div class="container clearfix">

                <!-- Logo
    ============================================= -->
                <div id="logo" class="divcenter">
                    <a href="/" class="standard-logo" data-dark-logo="/galerias/logo.png"><img src="/galerias/logo.png" alt="JPW TechDigital"></a>
                    <a href="/" class="retina-logo" data-dark-logo="/galerias/logo.png"><img src="/galerias/logo.png" alt="JPW TechDigital"></a>
                </div><!-- #logo end -->

            </div>

            <div id="header-wrap">

                <!-- Primary Navigation
				============================================= -->
                <nav id="primary-menu" class="style-2 center">

                    <div class="container clearfix">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>


                        <ul>
                            <li class="current"><a href="{{ url('/') }}">
                                    <div>Início</div>
                                </a></li>
                            <li class=""><a href="{{ url('/quem-somos') }}">
                                    <div>Quem Somos</div>
                                </a></li>
                            <li><a href="#">
                                    <div>Nossos Serviços <i class="fa fa-angle-down"></i></div>
                                </a>
                                <ul>
                                    <li><a href="{{ url('/desenvolvimento-sites') }}">
                                            <div>Desenvolvimento de Sites</div>
                                        </a></li>
                                    <li><a href="{{ url('/otimizacao-site-seo') }}">
                                            <div>Otimização de Site S.E.O</div>
                                        </a></li>
                                    <li><a href="{{ url('/campanha-google-ads') }}">
                                            <div>Campanha Google ADS</div>
                                        </a></li>
                                    <li><a href="{{ url('/marketing-conteudo') }}">
                                            <div>Marketing de Conteúdo</div>
                                        </a></li>
                                    <li><a href="{{ url('/registro-dominio') }}">
                                            <div>Registro de Domínio</div>
                                        </a></li>
                                    <li><a href="{{ url('/gerenciamento-erp') }}">
                                            <div>Gerenciamento de ERP</div>
                                        </a></li>
                                    <li><a href="{{ url('/administracao-b2wads') }}">
                                            <div>Administração B2WAds</div>
                                        </a></li>
                                    <li><a href="{{ url('/ecommerce-loja-virtual') }}">
                                            <div>Loja Virtual <small>E-commerce</small></div>
                                        </a></li>
                                </ul>
                            </li>
                            <li class=""><a href="{{ url('/portfolio') }}">
                                    <div>Portfólio</div>
                                </a></li>
                            <li class=""><a href="{{ url('/contato') }}">
                                    <div>Contato</div>
                                </a></li>
                        </ul>
                    </div>

                </nav>
            </div>

        </header><!-- #header end -->


        @yield('content')

        <!-- Footer
		============================================= -->
        <footer id="footer" class="dark">

            <!-- Copyrights
			============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Todos os Direitos Reservados @ <b>JPW Tech Digital</b>
                        <br>
                        CNPJ: 37.121.967/0001-31
                    </div>

                    <div class="col_half col_last tright d-none hide">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="#" class="social-icon si-small si-borderless si-instagram">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                            <a href="mailto:" class="social-icon si-small si-borderless si-gplus">
                                <i class="icon-comments"></i>
                                <i class="icon-comments"></i>
                            </a>
                        </div>

                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
	============================================= -->
    <script type="text/javascript" src="{{ asset('/frontend/js/jquery.js?2') }}"></script>
    <script type="text/javascript" src="{{ asset('/frontend/js/plugins.js?2') }}"></script>

    <!-- Footer Scripts
	============================================= -->
    <script type="text/javascript" src="{{ asset('/frontend/js/functions.js?1') }}"></script>

    @yield('jsPage')

    <script src="{{ asset('/frontend/js/jquery.maskedinput.js') }}"></script>
    <script>
        var $ = jQuery.noConflict();
        $("#side-navigation").tabs({
            show: {
                effect: "fade",
                duration: 400
            }
        });
        $('#phone').mask('(00) 0000-00009');
        $('#phone').blur(function(event) {
            if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                $('#phone').mask('(00) 0000-0000');
            } else {
                $('#phone').mask('(00) 90000-0000');
            }
        });
    </script>

    <!--Start of Tawk.to Script-->
    <script type='text/javascript'>
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement('script'),
                s0 = document.getElementsByTagName('script')[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5ee119a99e5f694422905097/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

</body>

</html>